use core::future::poll_fn;
use core::task::Poll;

use embassy_sync::waitqueue::WakerRegistration;
use embassy_sync::mutex::Mutex;
use embassy_sync::blocking_mutex::raw::NoopRawMutex;
use embassy_sync::signal::Signal;

use sunset::{Runner, Result, Behaviour};
use sunset::config::MAX_CHANNELS;

pub(crate) struct Inner<'a> {
    pub runner: Runner<'a>,

    pub chan_read_wakers: [WakerRegistration; MAX_CHANNELS],
    pub chan_write_wakers: [WakerRegistration; MAX_CHANNELS],
}

pub struct EmbassySunset<'a> {
    pub(crate) inner: Mutex<NoopRawMutex, Inner<'a>>,

    progress_notify: Signal<NoopRawMutex, ()>,
}

impl<'a> EmbassySunset<'a> {
    pub fn new(runner: Runner<'a>) -> Self {
        let inner = Inner {
            runner,
            chan_read_wakers: Default::default(),
            chan_write_wakers: Default::default(),
        };
        let inner = Mutex::new(inner);

        let progress_notify = Signal::new();

        Self {
            inner,
            progress_notify,
         }
    }

    pub async fn progress(&self,
        b: &mut Behaviour<'_>)
        -> Result<()> {
            let mut inner = self.inner.lock().await;
            inner.runner.progress(b).await?;

            if let Some((chan, _ext)) = inner.runner.ready_channel_input() {
                inner.chan_read_wakers[chan as usize].wake()
            }

            for chan in 0..MAX_CHANNELS {
                if inner.runner.ready_channel_send(chan as u32).unwrap_or(0) > 0 {
                    inner.chan_write_wakers[chan].wake()
                }
            }

            // idle until input is received
            // TODO do we also want to wake in other situations?
            self.progress_notify.wait().await;
            Ok(())
    }

    pub async fn read(&self, buf: &mut [u8]) -> Result<usize> {
        poll_fn(|cx| {
            let r = match self.inner.try_lock() {
                Ok(mut inner) => {
                    match inner.runner.output(buf) {
                        Ok(0) => {
                            inner.runner.set_output_waker(cx.waker());
                            Poll::Pending
                        }
                        Ok(n) => Poll::Ready(Ok(n)),
                        Err(e) => Poll::Ready(Err(e)),
                    }
                }
                Err(_) => Poll::Pending,
            };

            r
        })
        .await
    }

    pub async fn write(&self, buf: &[u8]) -> Result<usize> {
        poll_fn(|cx| {
            let r = match self.inner.try_lock() {
                Ok(mut inner) => {
                    if inner.runner.ready_input() {
                        match inner.runner.input(buf) {
                            Ok(0) => {
                                inner.runner.set_input_waker(cx.waker());
                                Poll::Pending
                            },
                            Ok(n) => Poll::Ready(Ok(n)),
                            Err(e) => Poll::Ready(Err(e)),
                        }
                    } else {
                        Poll::Pending
                    }
                }
                Err(_) => Poll::Pending,
            };

            if r.is_ready() {
                self.progress_notify.signal(())
            }
            r
        })
        .await
    }

    // pub async fn read_channel(&self, buf: &mut [u8]) -> Result<usize> {
}
